<?php

	/**
	 * Clase: Index
	 */
	class Index extends Controlador {

        /**
         * Index constructor.
         */
        function __Construct() {
            parent::__Construct();
            NeuralSesiones::Inicializar(APP);
            if(isset($_SESSION, $_SESSION['UOAUTH_APP']) == true){
                header('Location:'.NeuralRutasApp::RutaUrlAppModulo('Central'));
                exit();
            }
        }

		/**
		 * Metodo: Index
		 */
		public function Index() {
			/**
			 * Ejecutando el motor de plantillas
			 * Se muestra la plantilla HTML
			 */
			$Plantilla = new NeuralPlantillasTwig('AppCadi');
			$Plantilla->Parametro('Key',NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
			echo $Plantilla->MostrarPlantilla('Index.html');
		}

		public function Autenticacion(){
            if(isset($_POST) and empty($_POST) == false and isset($_POST["Key"]) and
                NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()){
                unset($_POST['Key']);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                $ConsultaUsuario = $this->Modelo->ConsultaUsuario($DatosPost['Usuario'], hash('sha256', $DatosPost['Password']));
                if($ConsultaUsuario['Cantidad'] == 1){
                    unset($DatosPost);
                    $this->RedireccionarUsuario($ConsultaUsuario[0]);
                }else{
                    unset($DatosPost,$ConsultaUsuario);
                    header("Location: ".NeuralRutasApp::RutaUrlApp('LogOut'));
                }
            }
        }

        private function RedireccionarUsuario($Consulta){
		    if(isset($Consulta) and empty($Consulta) == false){
                $ConsultaPermisos = $this->Modelo->ConsultarPermisos($Consulta['idPerfil']);
                if($ConsultaPermisos['Cantidad'] == 1){
                    AppSession::Registrar($Consulta,$ConsultaPermisos[0]);
                    unset($Consulta,$ConsultaPermisos);
                    header("Location: " .NeuralRutasApp::RutaUrlAppModulo('Central'));
                }
		    }
        }

	}