<?php
	
	/**
	 * Clase: Index_Modelo
	 */
	class Index_Modelo extends AppSQLConsultas {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Metodo: Ejemplo
		 */
		public function ConsultaUsuario($Usuario, $Password) {
            if($Usuario == true AND $Password    == true) {
                $Consulta = new NeuralBDConsultas(APP);
                $Consulta->Tabla('tbl_sistema_usuarios');
                $Consulta->Columnas(array_merge(self::ListarColumnas('tbl_informacion_usuarios', false, false, APP), self::ListarColumnas('tbl_sistema_usuarios', array('Password', 'Status'), false, APP)));
                $Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.IdUsuario');
                $Consulta->Condicion("(tbl_sistema_usuarios.Usuario = '$Usuario' OR tbl_informacion_usuarios.Correo = '$Usuario')");
                $Consulta->Condicion("tbl_sistema_usuarios.Password = '$Password'");
                $Consulta->Condicion("tbl_sistema_usuarios.Status = 'ACTIVO'");
                return $Consulta->Ejecutar(true, true);
            }
		}

		public function ConsultarPermisos($idPerfil){
		    if($idPerfil == true){
		        $Consulta = new NeuralBDConsultas(APP);
		        $Consulta-> Tabla('tbl_sistema_usuarios_perfil');
		        $Consulta->Columnas(self::ListarColumnas('tbl_sistema_usuarios_perfil', array('idPerfil','Status'),false,APP));
                $Consulta-> Condicion("idPerfil = '$idPerfil'");
		        return $Consulta -> Ejecutar(true, true);
            }
        }
	}